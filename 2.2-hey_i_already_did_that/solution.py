def answer(n, b):
    n = str(n)
    n_list = [n]
    end_cycle_length = None
    
    def base_n(decimal, base):
        """
        Decimal to any base
        """
        nums = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        ans = ""
        while decimal != 0:
            ans += nums[decimal % base]
            decimal /= base
        return ans[::-1]
    
    def get_next_n(number, base):
        """
        Get next n value
        """
        k = len(number)
        x = int("".join(sorted(number, reverse=True)), base)
        y = int("".join(sorted(number)), base)
        z = x-y
        z_base_str = str(base_n(z, base)).zfill(k)
        return z_base_str
    
    while True:
        next_n = get_next_n(n_list[-1], b)
        
        if next_n in n_list:
            first_occurance = n_list.index(next_n)
            last_occurance = len(n_list)
            end_cycle_length = last_occurance - first_occurance
            break
        else:
            n_list.append(next_n)

    return end_cycle_length
