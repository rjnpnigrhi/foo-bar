import datetime
start_time = datetime.datetime.now()

data1 = [1, 2, 3]
n1 = 0

data2 = [1, 2, 2, 3, 3, 3, 4, 5, 5]
n2 = 1

data3 = [1, 2, 3]
n3 = 6


def answer(data, n):
    data_set = frozenset(data)
    for d in data_set:
        if data.count(d) > n:
            data = filter(lambda a: a != d, data)
    return data

    # using list comprehension but not optimal
    # return [x for x in data if x not in [y for y in frozenset(data) if data.count(y) > n]]

ans1 = answer(data=data1, n=n1)
ans2 = answer(data=data2, n=n2)
ans3 = answer(data=data3, n=n3)
ans4 = answer(data=range(1000), n=1)

print(ans1)
print(ans2)
print(ans3)
print(ans4)

end_time = datetime.datetime.now()
time_taken = end_time - start_time

print("program took", time_taken.microseconds, "micro seconds to run")
