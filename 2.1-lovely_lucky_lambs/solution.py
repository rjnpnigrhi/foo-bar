def​ ​answer(total_lambs):
​ ​​ ​​ ​​ ​
​ ​​ ​​ ​​ ​def​ ​generous_henchmen_count(total):
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​"""
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​Calculates​ ​the​ ​number​ ​of​ ​henchmen​ ​if​ ​lambs​ ​were​ ​given​ ​generously
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​"""
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​second_sub_lambs​ ​=​ ​None
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​first_sub_lambs​ ​=​ ​None
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​total_dist_lambs​ ​=​ ​1
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​senior_lambs​ ​=​ ​1
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​min_henchmen_count​ ​=​ ​1
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​while​ ​total_dist_lambs​ ​<=​ ​total:
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​second_sub_lambs​ ​=​ ​first_sub_lambs
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​first_sub_lambs​ ​=​ ​senior_lambs
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​senior_lambs​ ​=​ ​first_sub_lambs​ ​*​ ​2
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​total_dist_lambs​ ​=​ ​total_dist_lambs​ ​+​ ​senior_lambs
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​if​ ​total_dist_lambs​ ​>​ ​total:
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​last_rem_lambs​ ​=​ ​total​ ​-​ ​(total_dist_lambs​ ​-​ ​senior_lambs)
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​#​ ​If​ ​enough​ ​number​ ​of​ ​lambs​ ​are​ ​present​ ​for​ ​senior​ ​most​ ​even​ ​if​ ​not​ ​in​ ​GP
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​if​ ​senior_lambs​ ​>​ ​last_rem_lambs​ ​>=​ ​first_sub_lambs​ ​+​ ​second_sub_lambs:
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​min_henchmen_count​ ​+=​ ​1
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​break
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​else:
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​min_henchmen_count​ ​+=​ ​1
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​return​ ​min_henchmen_count

​ ​​ ​​ ​​ ​def​ ​stingy_henchmen_count(total):
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​"""
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​Calculates​ ​the​ ​number​ ​of​ ​henchmen​ ​if​ ​lambs​ ​were​ ​given​ ​stringily
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​"""
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​second_sub_lambs​ ​=​ ​None
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​first_sub_lambs​ ​=​ ​None
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​total_dist_lambs​ ​=​ ​1
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​senior_lambs​ ​=​ ​1
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​max_henchmen_count​ ​=​ ​1
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​while​ ​total_dist_lambs​ ​<=​ ​total:
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​second_sub_lambs​ ​=​ ​first_sub_lambs
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​first_sub_lambs​ ​=​ ​senior_lambs
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​senior_lambs​ ​=​ ​first_sub_lambs​ ​if​ ​not​ ​second_sub_lambs​ ​else​ ​first_sub_lambs​ ​+​ ​second_sub_lambs
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​total_dist_lambs​ ​=​ ​total_dist_lambs​ ​+​ ​senior_lambs

​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​if​ ​total_dist_lambs​ ​<=​ ​total:
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​max_henchmen_count​ ​+=​ ​1

​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​return​ ​max_henchmen_count

​ ​​ ​​ ​​ ​if​ ​10​ ​<=​ ​total_lambs​ ​<=​ ​10​ ​**​ ​9:
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​return​ ​stingy_henchmen_count(total_lambs)​ ​-​ ​generous_henchmen_count(total_lambs)
​ ​​ ​​ ​​ ​else:
​ ​​ ​​ ​​ ​​ ​​ ​​ ​​ ​return​ ​0
