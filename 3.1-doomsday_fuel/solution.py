from __future__ import division
from fractions import Fraction
from functools import reduce


def standard_form_matrix(trans_matrix, terminal_states):
    stan_matrix = []

    # terminal states
    for i in range(len(terminal_states)):
        if terminal_states[i]:
            stan_matrix.append(trans_matrix[i])

    operations_list = []
    non_terminal_list = []
    for j in range(len(terminal_states)):
        if not terminal_states[j]:
            row = trans_matrix[j]
            non_terminal_list.append(row)
            operations_list.append(j)

    # non terminal states
    for x in range(len(non_terminal_list)):
        row = non_terminal_list[x]
        new_row = []
        for y in range(len(row)):
            item = row[y]
            if y not in operations_list:
                new_row.append(item)

        for z in operations_list:
            new_row.append(row[z])

        stan_matrix.append(new_row)

    return stan_matrix


def probability_matrix(trans_matrix):
    prob_matrix = []
    for i in range(len(trans_matrix)):
        row = trans_matrix[i]
        new_row = []
        row_sum = sum(trans_matrix[i])
        if all([v == 0 for v in trans_matrix[i]]):
            for j in trans_matrix[i]:
                new_row.append(0)
            new_row[i] = 1
            prob_matrix.append(new_row)
        else:
            for j in trans_matrix[i]:
                if j == 0:
                    new_row.append(0)
                else:
                    new_row.append(j / row_sum)
            prob_matrix.append(new_row)
    return prob_matrix


def terminal_states_list(trans_matrix):
    tsl = []
    for row in range(len(trans_matrix)):
        if all(x == 0 for x in trans_matrix[row]):
            tsl.append(True)
        else:
            tsl.append(False)
    return tsl


def get_q(prob_matrix, terminal_state_count):
    non_ts = prob_matrix[terminal_state_count:]
    q = []
    for nts in non_ts:
        new_row = nts[terminal_state_count:]
        q.append(new_row)
    return q


def get_r(prob_matrix, terminal_state_count):
    non_ts = prob_matrix[terminal_state_count:]
    r = []
    for nts in non_ts:
        new_row = nts[:terminal_state_count]
        r.append(new_row)
    return r


def identity(n):
    return [[1 if i == j else 0 for j in range(n)] for i in range(n)]


def zero_matrix(n):
    return [[0 for j in range(n)] for i in range(n)]


def subtract(matrix1, matrix2):
    result = zero_matrix(len(matrix1))
    for i in range(len(matrix1)):
        for j in range(len(matrix1[i])):
            result[i][j] = matrix1[i][j] - matrix2[i][j]
    return result


def get_matrix_inverse(m):
    def transpose_matrix(m):
        t = []
        for m_row in range(len(m)):
            t_row = []
            for c in range(len(m[m_row])):
                if c == m_row:
                    t_row.append(m[m_row][c])
                else:
                    t_row.append(m[c][m_row])
            t.append(t_row)
        return t

    def get_matrix_minor(matrix, i, j):
        return [m_row[:j] + m_row[j + 1:] for m_row in (matrix[:i] + matrix[i + 1:])]

    def get_matrix_deternminant(m):
        if len(m) == 2:
            return m[0][0] * m[1][1] - m[0][1] * m[1][0]

        d = 0
        for m_row in range(len(m)):
            d += ((-1) ** m_row) * m[0][m_row] * get_matrix_deternminant(get_matrix_minor(m, 0, m_row))
        return d

    determinant = get_matrix_deternminant(m)
    if len(m) == 2:
        return [[m[1][1] / determinant, -1 * m[0][1] / determinant],
                [-1 * m[1][0] / determinant, m[0][0] / determinant]]

    co_factors = []
    for row in range(len(m)):
        co_factor_row = []
        for c in range(len(m)):
            minor = get_matrix_minor(m, row, c)
            co_factor_row.append(((-1) ** (row + c)) * get_matrix_deternminant(minor))
        co_factors.append(co_factor_row)
    co_factors = transpose_matrix(co_factors)

    for row in range(len(co_factors)):
        for c in range(len(co_factors)):
            co_factors[row][c] = co_factors[row][c] / determinant
    return co_factors


def matrix_multi(m1, m2):
    res = []
    m = []
    for i in range(len(m1)):
        for j in range(len(m2[0])):
            sums = 0
            for k in range(len(m2)):
                sums = sums + (m1[i][k] * m2[k][j])
            res.append(sums)
        m.append(res)
        res = []
    return m


def get_lcm_for(list_items):
    def lcm(a, b):
        if a > b:
            greater = a
        else:
            greater = b

        while True:
            if greater % a == 0 and greater % b == 0:
                lcm_a_b = greater
                break
            greater += 1

        return lcm_a_b

    return reduce(lambda x, y: lcm(x, y), list_items)


def answer(m):
    
    # initial terminal states list
    initial_terminal_states = terminal_states_list(m)

    # standard representation of markov chains
    stan_matrix = standard_form_matrix(m, initial_terminal_states)

    # standard representation of probability matrix
    prob_matrix = probability_matrix(stan_matrix)

    terminal_state_count = initial_terminal_states.count(True)
    non_terminal_state_count = initial_terminal_states.count(False)

    ###############################
    # Transition matrix:
    #
    # P =   |       |       |
    #       |   I   |   0   |
    #       |_______________|
    #       |       |       |
    #       |   R   |   Q   |
    #       |       |       |
    #
    #
    ###############################

    ###############################
    # Terminal matrix:
    # _
    # P =   |       |       |
    #       |   I   |   0   |
    #       |_______________|
    #       |       |       |
    #       |   FR  |   0   |
    #       |       |       |
    #
    #   F = (I - Q)^-1
    #
    ###############################

    q = get_q(prob_matrix, terminal_state_count)
    r = get_r(prob_matrix, terminal_state_count)

    i = identity(non_terminal_state_count)
    i_q = subtract(i, q)
    f = get_matrix_inverse(i_q)
    f_r = matrix_multi(f, r)

    if non_terminal_state_count == 0:
        # if there is no non-terminal state then,
        # probability of reaching terminal state is 100%
        # because it is already in terminal state
        steps = [1]
    else:
        # because `So` always stays at the top of FR
        steps = f_r[0]

    numerators = []
    denominators = []

    for i in steps:
        res = Fraction(i).limit_denominator()
        numerators.append(res.numerator)
        denominators.append(res.denominator)

    denominator_lcm = get_lcm_for(denominators)

    ans = []
    for j in steps:
        val = int(round(denominator_lcm*j))
        ans.append(val)

    ans.append(denominator_lcm)

    return ans


# answer([
#     [0, 2, 1, 0, 0],
#     [0, 0, 0, 3, 4],
#     [0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0]
# ])

# answer([
#     [0, 1, 0, 0, 0, 1],
#     [4, 0, 0, 3, 2, 0],
#     [0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0]
# ])

# answer([
#     [0, 7, 0, 17, 0, 1, 0, 5, 0, 2],
#     [0, 0, 29, 0, 28, 0, 3, 0, 16, 0],
#     [0, 3, 0, 0, 0, 1, 0, 0, 0, 0],
#     [48, 0, 3, 0, 0, 0, 17, 0, 0, 0],
#     [0, 6, 0, 0, 0, 1, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
# ])

# answer([
#     [0]
# ])



assert (
    answer([
        [0, 2, 1, 0, 0],
        [0, 0, 0, 3, 4],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ]) == [7, 6, 8, 21]
)

assert (
    answer([
        [0, 1, 0, 0, 0, 1],
        [4, 0, 0, 3, 2, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]) == [0, 3, 2, 9, 14]
)

assert (
    answer([
        [1, 2, 3, 0, 0, 0],
        [4, 5, 6, 0, 0, 0],
        [7, 8, 9, 1, 0, 0],
        [0, 0, 0, 0, 1, 2],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]) == [1, 2, 3]
)
assert (
    answer([
        [0]
    ]) == [1, 1]
)

assert (
    answer([
        [0, 0, 12, 0, 15, 0, 0, 0, 1, 8],
        [0, 0, 60, 0, 0, 7, 13, 0, 0, 0],
        [0, 15, 0, 8, 7, 0, 0, 1, 9, 0],
        [23, 0, 0, 0, 0, 1, 0, 0, 0, 0],
        [37, 35, 0, 0, 0, 0, 3, 21, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]) == [1, 2, 3, 4, 5, 15]
)

assert (
    answer([
        [0, 7, 0, 17, 0, 1, 0, 5, 0, 2],
        [0, 0, 29, 0, 28, 0, 3, 0, 16, 0],
        [0, 3, 0, 0, 0, 1, 0, 0, 0, 0],
        [48, 0, 3, 0, 0, 0, 17, 0, 0, 0],
        [0, 6, 0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]) == [4, 5, 5, 4, 2, 20]
)

assert (
    answer([
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]) == [1, 1, 1, 1, 1, 5]
)

assert (
    answer([
        [1, 1, 1, 0, 1, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 1, 1, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 1, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]) == [2, 1, 1, 1, 1, 6]
)

assert (
    answer([
        [0, 86, 61, 189, 0, 18, 12, 33, 66, 39],
        [0, 0, 2, 0, 0, 1, 0, 0, 0, 0],
        [15, 187, 0, 0, 18, 23, 0, 0, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]) == [6, 44, 4, 11, 22, 13, 100]
)

assert (
    answer([
        [0, 0, 0, 0, 3, 5, 0, 0, 0, 2],
        [0, 0, 4, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 4, 4, 0, 0, 0, 1, 1],
        [13, 0, 0, 0, 0, 0, 2, 0, 0, 0],
        [0, 1, 8, 7, 0, 0, 0, 1, 3, 0],
        [1, 7, 0, 0, 0, 0, 0, 2, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]) == [1, 1, 1, 2, 5]
)